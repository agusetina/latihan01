package agustinaagustus;

public class DeretFibbonnaci {
    public static void main(String[] args) {

        nilaiDeret(8);
    }
    static void nilaiDeret (int n){
        int d, d_1,d_2;
        d_1 = 1;
        d_2 = 0;

        for (int i = 1; i <= n; i++) {

            d = d_1 + d_2;
            System.out.print(" " + d);
            d_2 = d_1;
            d_1 = d;
        }
    }
}
